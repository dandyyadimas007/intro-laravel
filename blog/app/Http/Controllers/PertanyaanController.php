<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('question.pertanyaan');
    }

    public function store(Request $request){
        //dd($request->all());
        $request ->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => $request["tanggal_dibuat"],
            "tanggal_diperbaharui" => $request["tanggal_diperbaharui"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan!');
    }

    public function index(){
        $tables = DB::table('pertanyaan')->get();
        //dd($tabel);
        return view('question.index', compact('tables'));
    }

    public function show($id){
        $show = DB::table('pertanyaan')->where('id', $id)->first();
        return view('question.show', compact('show'));
    }

    public function edit($id){
        $show = DB::table('pertanyaan')->where('id', $id)->first();
        return view('question.edit', compact('show'));
    }

    public function update($id, Request $request){
        $request ->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);
        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' =>$request['judul'],
                        'isi' => $request['isi'],
                        'tanggal_dibuat' => $request['tanggal_dibuat'],
                        'tanggal_diperbaharui'=> $request['tanggal_diperbaharui']
                    ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil Update Pertanyaan!');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus!');
    }
}
