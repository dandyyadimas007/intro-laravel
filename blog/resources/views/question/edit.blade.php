@extends('adminlte.master');

@section('content')
    <div class="ml-3 mt-3">
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Edit Pertanyaan {{$show->id}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan/{{$show->id}}" method="POST">
                @csrf
                @method('PUT')
            <div class="card-body">
                <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $show->judul)}}"placeholder="Judul" required>
                </div>
            @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
                <div class="form-group">
                <label for="isi">Isi</label>
                <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $show->isi)}}" placeholder="Isi" required>
                </div>
            @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
                <div class="form-group">
                <label for="tanggal_dibuat">Tanggal Dibuat</label>
                <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat" placeholder="Tanggal Dibuat" required>
                </div>
            @error('tanggal_dibuat')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
                <div class="form-group">
                <label for="tanggal_diperbaharui">Tanggal Diperbaharui</label>
                <input type="date" class="form-control" id="tanggal_diperbaharui" name="tanggal_diperbaharui" value="{{ $show->tanggal_diperbaharui }}"placeholder="Tanggal Diperbaharui" required>
                </div>
            @error('tanggal_diperbaharui')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
            </form>
        </div>
    </div>
@endsection