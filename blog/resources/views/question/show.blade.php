@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <h4>{{ $show->judul }}</h4>
    <p>{{ $show->isi }}</p>
    <p>{{ $show->tanggal_dibuat }}</p>
    <p>{{ $show->tanggal_diperbaharui }}</p>
</div>
@endsection