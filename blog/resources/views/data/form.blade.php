<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <h2>Sign Up Form</h2>
    <form action="/login" method="POST">
        @csrf 
        
        <label for="first">First name:</label> <br><br>
        <input type="text" placeholder="First name" name="first">
        <br><br>
        <label for="last">Last name:</label> <br><br>
        <input type="text" placeholder="Last name" name="last">
        <br><br>
        
        <label>Gender:</label> <br>
        <input type="radio" name="gender" value="0">Male <br>
        <input type="radio" name="gender" value="1">Female <br>
        <input type="radio" name="gender" value="2">Other <br><br>
        
        <label>Nationality</label> <br>
        <select>
            <option>Indonesian</option> <br>
            <option>Singapore</option> <br>
            <option>USA</option> <br>
            <option>Russian</option>
            <option>Others</option>
        </select> <br><br>

        <label>Language Spoken</label> <br>
        <input type="checkbox" name="language" value="1">Indonesia <br>
        <input type="checkbox" name="language" value="2">English <br>
        <input type="checkbox" name="language" value="3">Other <br><br>
        
        <label>Bio</label> <br>
        <textarea cols="25" rows="10"></textarea> <br><br>
        
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>